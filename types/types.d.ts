export type PostType = {
    id: number;
    userId: number;
    title: string;
    body: string;
    date?: Date;
}

export type PostInitialState = {
    posts: PostType[],
    isLoading: boolean
}

export type ModeInitialState = {
    isDarkMode: boolean
}

export type PostParamsType = {
    id: number
}