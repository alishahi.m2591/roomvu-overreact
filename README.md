Roomvu Task Challenge :)

## Getting Started

This Challenge is made by Nextjs 13 ,react-redux and tailwindcss.

run next commands to see the result

```bash
npm install
# then
npm run dev
# or
yarn
# then
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
