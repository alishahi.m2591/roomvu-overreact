import Image from 'next/image'
import Link from 'next/link';

export const Author = () => {

    return (
        <div className='flex flex-row justify-start items-center mb-14'>
            <Image
                src={'/profile.jpg'}
                width={56}
                height={56}
                className='rounded-full mr-3'
                alt="Dan Abramov"
            />
            <div className='flex flex-col justify-center items-center max-w-xs font-normal'>
                <p>Personal blog by <Link className='text-[#ffa7c4] underline' href="https://mobile.twitter.com/dan_abramov">Dan Abramov</Link>.</p>
                <p>I explain with words and code.</p>
            </div>
        </div>
    );
}
