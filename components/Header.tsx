
import { ThemeSwitcher } from './ThemeSwitcher';

export const Header = ({ title }: { title: string }) => {

  return (
    <div className='flex flex-row justify-between items-center mb-11 text-xl'>
      <h1 className='text-4xl font-black font-serif'>{title}</h1>
      <ThemeSwitcher />
    </div>
  );
}
