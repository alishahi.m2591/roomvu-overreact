import Image from 'next/image'
import Link from 'next/link';

export const InfoView = ({ message = 'Some thing went wrong' }: { message: string }) => {

    return (
        <div className='flex justify-center items-center'>
            <h1 className='dark:text-[#ffa7c4] text-4xl font-black'>{message}</h1>
        </div>
    );
}
