import Link from 'next/link';
import { PostType } from '@/types/types';
import dateFormatter from '@/utils/dateFormatter';


export const PostsTile = ({ post: { id, title, date, body } }: { post: PostType }) => {

    return (
        <div className='mb-11 '>
            <Link href={`/post/${id}`} >
                <h3 className='text-4xl font-black font-serif text-[#d23669] dark:text-[#ffa7c4] mb-4'>{title}</h3>
                {date && <p className='text-sm font-normal font-sans'>{dateFormatter(date)}</p>}
            </Link>
            <p className='mt-3 text-2xl font-sans font-normal'>{body}</p>
        </div >
    );
}
