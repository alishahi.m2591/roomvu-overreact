'use client'

import { useDispatch, useSelector } from 'react-redux';
import { FaMoon, FaSun } from 'react-icons/fa';
import Switch from 'react-switch';
import { AppDispatch, RootState } from '../store/store'
import { toggleDarkMode } from '../store/slices/themeSlice';


export const ThemeSwitcher = () => {
  const dispatch = useDispatch<AppDispatch>();
  const { isDarkMode } = useSelector((state: RootState) => state.theme);

  const handleToggle = () => {
    dispatch(toggleDarkMode());
    if (!isDarkMode) {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }
  };

  return (
    <Switch
      checked={isDarkMode}
      onChange={handleToggle}
      onColor="#0f1114"
      offColor="#0f1114"
      onHandleColor="#f8fafc"
      offHandleColor="#f8fafc"
      handleDiameter={26}
      width={55}
      height={24}
      activeBoxShadow='0 0 2px 3px #ffa7c4'
      boxShadow='0 0 2px 3px #ffa7c4'
      uncheckedIcon={<FaSun className="text-yellow-500 h-6 ml-1" />}
      checkedIcon={<FaMoon className="text-yellow-500  h-6 mr-1" />}
    />
  );
}
