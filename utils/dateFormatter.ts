const dateFormatter = (date: Date) => {

    const formattedDate: string = date.toLocaleDateString('en-US', {
        month: 'long',
        day: 'numeric',
        year: 'numeric',
    });

    return formattedDate
}

export default dateFormatter;