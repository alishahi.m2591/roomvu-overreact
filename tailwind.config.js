/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'dark-bg': '#282c35',
        'dark-text': '#fff',
      },
    },
    fontFamily: {
      sans: ['Merriweather', 'Georgia', 'serif'],
      serif: ['Montserrat', 'sans-serif'],
    },
  },
  variants: {},
  plugins: [],
  images: {
    domains: ['overreacted.io'],
  },
}
