'use client'

import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { AppDispatch, RootState } from '../store/store'
import { fetchPosts } from '../store/slices/postSlice'
import { PostType } from '@/types/types'
import { Author } from '@/components/Author'
import { InfoView } from '@/components/InfoView'
import { PostsTile } from '@/components/PostsTile'



export default function Home() {
  const { posts, isLoading } = useSelector((state: RootState) => state.post)
  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    dispatch(fetchPosts())
  }, [])

  if (isLoading) return <InfoView message='Loading...' />

  return (
    <main>
      <Author />
      {posts && posts.map((post: PostType) => (<PostsTile key={post.id} post={post} />))}
    </main>
  );
}
