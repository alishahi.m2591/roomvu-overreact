'use client'

import React, { useEffect } from 'react'
import { PostParamsType, PostType } from '@/types/types'
import { useSelector, useDispatch } from 'react-redux'
import { AppDispatch, RootState } from '@/store/store'
import { fetchPosts } from '@/store/slices/postSlice'
import { InfoView } from '@/components/InfoView'
import dateFormatter from '@/utils/dateFormatter'

const showMessageView = (id: number, post: PostType | undefined, isLoading: boolean) => {
  let message = ''
  if (isLoading) {
    message = 'Loading...'
  } else if (!post) {
    message = 'No post with this id'
  } else if (!id) {
    message = 'Select a post!'
  }
  return message
}

export default function Post({ params }: { params: PostParamsType }) {
  const { posts, isLoading } = useSelector((state: RootState) => state.post)

  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    if (posts.length) return

    dispatch(fetchPosts())
  }, [])

  const post = posts.find(post => post.id == params.id)

  const message = showMessageView(params.id, post, isLoading)

  if (message || !post) return <InfoView message={message} />

  const { body, title, date } = post

  return (
    <main className="flex flex-col items-start justify-center mb-4">
      <div className='mb-3'>
        <h1 className='font-serif font-black text-[#000] dark:text-[#fff] text-4xl mb-4'>{title}</h1>
        {date && <p className='font-sans text-[#000] dark:text-[#fff] font-sm'>{dateFormatter(date)}</p>}
      </div>
      <div className='font-sans text-[#000] dark:text-[#fff]'>
        {body}
      </div>
    </main>
  )
}