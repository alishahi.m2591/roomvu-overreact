'use client'

import './globals.css'
import type { Metadata } from 'next'
import { Providers } from './provider'
import { Header } from '../components/Header'

export const metadata: Metadata = {
  title: 'Task for roomvu',
  description: 'we make content, to help you make money!',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <Providers>
        <body className='bg-white dark:bg-gray-900 dark:text-white font-sans'>
          <div className='max-w-3xl mx-auto my-11 min-h-screen'>
            <Header title='Overreacted' />
            {children}
          </div>
        </body>
      </Providers>
    </html>
  )
}
