import { configureStore } from '@reduxjs/toolkit';
import postReducer from './slices/postSlice'
import themeReducer from './slices/themeSlice'

export const store = configureStore({
    reducer: {
        post: postReducer,
        theme: themeReducer,
    }
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;