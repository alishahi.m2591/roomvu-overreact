import { PostInitialState, PostType } from '@/types/types';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchPosts = createAsyncThunk('posts/getAllPosts', async (thunkApi) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts')
    const data: PostType[] = await response.json();

    data.forEach((post: PostType) => {
        const postId = post.id
        const currentDate = new Date();
        const date = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - postId);

        post.date = date;
    });
    const sortedPosts: PostType[] = data.sort((a, b) => (b.date as Date) - (a.date as Date));

    return sortedPosts;
})

const initialState = {
    posts: [],
    isLoading: true
} as PostInitialState

const postSlice = createSlice({
    name: 'posts',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchPosts.fulfilled, (state, action) => {
            state.isLoading = false
            state.posts.push(...action.payload)
        })

        builder.addCase(fetchPosts.pending, (state, action) => {
            state.isLoading = true
        })
    }
})

export default postSlice.reducer